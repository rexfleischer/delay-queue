package queue

import org.junit.Assert
import org.junit.Test
import other.Promise
import java.util.concurrent.ConcurrentHashMap

class SimpleQueueTests : InstancePerTest() {
  
  @Test
  fun testHappyPath() {
    val promise = Promise<String>()
    system.addInProcHandler("hello") {
      promise.complete(it.base64Payload)
      Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Success) }
    }
    
    system.queueMessage(QueueRequest("hello", 0, "world")).join()
    Assert.assertEquals("world", promise.join())
  }
  
  @Test
  fun testHappyPath2() {
    val gets = ConcurrentHashMap.newKeySet<String>()
    
    system.addInProcHandler("hello") {
      gets += it.base64Payload
      Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Success) }
    }
    
    system.queueMessage(QueueRequest("hello", 0, "world1")).join()
    system.queueMessage(QueueRequest("hello", 0, "world2")).join()
    system.queueMessage(QueueRequest("hello", 0, "world3")).join()
    system.queueMessage(QueueRequest("hello", 0, "world4")).join()
    
    waitUntil { gets.size == 4 }
  }
  
  @Test
  fun testHappyPathStress() {
    val gets = ConcurrentHashMap.newKeySet<String>()
    
    system.addInProcHandler("hello") {
      gets += it.base64Payload
      Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Success) }
    }
    
    (1..100).forEach { system.queueMessage(QueueRequest("hello", 0, "world$it")) }
    
    waitUntil { gets.size == 100 }
  }
  
  @Test
  fun testHappyPathStress2() {
    val gets = ConcurrentHashMap.newKeySet<String>()
    
    system.addInProcHandler("hello") {
      gets += it.base64Payload
      Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Success) }
    }
    
    val count = 10000
    (1..count).map { system.queueMessage(QueueRequest("hello", 0, "world$it")) }.forEach { it.join() }
    
    println("stuff")
    waitUntil(timeout = 10_000) { gets.size == count }
    println("stuff2")
  }
}
