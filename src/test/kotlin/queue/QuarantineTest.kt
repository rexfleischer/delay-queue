package queue

import org.junit.Test
import other.Promise
import java.util.concurrent.ConcurrentHashMap

class QuarantineTest : InstancePerTest() {
  
  @Test
  fun testOneErrorRetried() {
    val gets = ConcurrentHashMap.newKeySet<String>()
  
    system.addInProcHandler("hello") {
      if (gets.size == 0) {
        println("stuffthings: 1")
        gets += it.base64Payload
        Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Failure("test", 200)) }
      }
      else {
        println("stuffthings: 2")
        gets += it.base64Payload + "1"
        Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Success) }
      }
    }
  
    system.queueMessage(QueueRequest("hello", 0, "world1")).join()
  
    waitUntil { gets.size == 2 }
  }
}