package queue

import org.junit.Test
import other.Promise
import other.currentTime
import java.util.Properties
import java.util.concurrent.ConcurrentHashMap


class PersistentQueueTest : InstancePerTest() {
  
  override fun buildQueue(): QueueSystem {
    val props = Properties()
    props.setProperty(POLLER_POLL_DELAY.name, "3000")
    props.setProperty(DELIVERY_POLL_TIME.name, "5000")
    return QueueSystem(props)
  }
  
  @Test
  fun testPersistentPoll() {
    val gets = ConcurrentHashMap.newKeySet<String>()
    
    system.addInProcHandler("hello") {
      println("hello from: $it")
      gets += it.base64Payload
      Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Success) }
    }
    
    // because of how we are configured above, this forces some of the messages
    // to get polled from the db.
    val time = currentTime()
    val count = 10
    (1..count).map { system.queueMessage(QueueRequest("hello", time + it * 1000, "world$it")) }.forEach { it.join() }
    
    println("stuff")
    waitUntil(timeout = count * 1000L + 1000L) { gets.size == count }
    println("stuff2")
  }
}
