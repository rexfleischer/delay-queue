package queue

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import org.junit.After
import org.junit.Before
import org.slf4j.LoggerFactory
import other.Promise
import other.SimpleService
import other.currentTime
import java.util.Properties

class CleanSystem(raw_config: Properties) : SimpleService(raw_config) {
  override fun createModules(): List<Kodein.Module> = listOf(
      injectMongoPersistence()
  )
}

fun waitUntil(timeout: Long = 5000, sleep: Long = 200, block: () -> Boolean) {
  val end_at = currentTime() + timeout
  while (currentTime() < end_at) {
    if (block()) return
    Thread.sleep(sleep)
  }
  if (!block()) throw IllegalStateException("timeout in wait block")
}


open class InstancePerTest {
  val system by lazy { buildQueue() }
  
  open fun buildQueue(): QueueSystem {
    return QueueSystem(Properties())
  }
  
  @Before
  fun before() {
    try {
      val cleaner = CleanSystem(Properties())
      cleaner.start().join()
      Promise<Unit>().also { cleaner.kodein.instance<MongoManager>().database.drop(it.proxyMongoUnitCallback()) }
      cleaner.shutdown().join()
      system.start().join()
    }
    catch (ex: Throwable) {
      LoggerFactory.getLogger("ohno").error("stuff", ex)
      throw ex
    }
  }
  
  @After
  fun after() {
    try {
      system.shutdown().join()
    }
    catch (ex: Throwable) {
      LoggerFactory.getLogger("ohcrap").error("stuff", ex)
    }
  }
}
