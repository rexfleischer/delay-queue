package queue

import org.junit.Assert
import org.junit.Test
import other.Promise
import java.util.Properties

class StartAndStopALot {
  // there's been issues with the actors not starting/stopping
  // correctly (hanging), so we're testing the start and stop
  @Test
  fun testStartAndStopALot() {
    (1..10).forEach {
      val system = QueueSystem(Properties())
      system.start().join()
      val promise = Promise<String>()
      system.addInProcHandler("hello") {
        promise.complete(it.base64Payload)
        Promise<MessageHandleResult>().also { it.complete(MessageHandleResult.Success) }
      }
      system.queueMessage(QueueRequest("hello", 0, "world")).join()
      Assert.assertEquals("world", promise.join())
      
      system.shutdown().join()
    }
  }
}
