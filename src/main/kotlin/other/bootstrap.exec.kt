package other

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance


/**
 * convenience extension method for bootstrapping in a builder
 */
fun Kodein.Builder.onCommand(command: String, step_name: String, order: Int = 0, hook: suspend Kodein.() -> Unit) {
  onReady {
    instance<CommandRegistry>().getEnsure(command).registerSuspend(order, step_name, { hook(this) })
  }
}

/**
 * command for the start process of the system
 */
fun Kodein.Builder.onStart(step_name: String, order: Int = 0, hook: suspend Kodein.() -> Unit) =
    onCommand("start", step_name, order, hook)

/**
 * command for the shutdown process of the system
 */
fun Kodein.Builder.onShutdown(step_name: String, order: Int = 0, hook: suspend Kodein.() -> Unit) =
    onCommand("shutdown", step_name, order, hook)
