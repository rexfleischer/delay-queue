package other

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import java.util.stream.Collectors

/**
 * this class is used to register and order suspend actions. this
 * is the workhorse for ordered executions of actions that need to
 * happen in an order.
 */
class TransitionAction(val name: String) {
  companion object {
    private val logger = LoggerFactory.getLogger(TransitionAction::class.java)
  }
  
  private val accepting = AtomicBoolean(true)
  private val registered = HashMap<Int, MutableList<Pair<String, () -> Promise<Unit>>>>()
  
  @Synchronized
  fun registerPromise(order: Int, name: String, hook: () -> Promise<Unit>) {
    if (!accepting.get()) throw IllegalStateException("unable to register hook after first execution")
    registered.computeIfAbsent(order, { ArrayList() }).add(name to hook)
  }
  
  @Synchronized
  fun registerSuspend(order: Int, name: String, hook: suspend () -> Unit) {
    if (!accepting.get()) throw IllegalStateException("unable to register hook after first execution")
    registered.computeIfAbsent(order, { ArrayList() }).add(name to { future { hook() } })
  }
  
  suspend fun execute(until: Int = Int.MAX_VALUE) {
    accepting.set(false)
    val executing = registered.entries.stream()
        .filter { entry -> entry.key <= until }
        .sorted { l, r -> l.key - r.key }
        .map { it.value }
        .collect(Collectors.toList())
    logger.info("executing {} with {} steps", name, executing.size)
    val start = System.currentTimeMillis()
    executing.forEachIndexed { index, execs ->
      val stage_start = currentTime()
      try {
        logger.debug("executing (${index + 1} / ${executing.size}): ${execs.map { it.first }}")
        execs.map {
          future {
            val step_start = currentTime()
            it.second().await()
            logger.debug("executing (${index + 1} / ${executing.size}): ${it.first} finished in ${currentTime() - step_start}ms")
          }
        }.forEach { it.await() }
      }
      catch (ex: Exception) {
        logger.error("unable to perform action $name", ex)
        throw ex
      }
      logger.debug("executing (${index + 1} / ${executing.size}): stage finished in ${currentTime() - stage_start}ms")
    }
    logger.info("finished executing {} in {}ms", name, (System.currentTimeMillis() - start))
  }
}

/**
 * the general way to actually execute a command
 */
fun Kodein.executeBootstrap(command: String): Promise<Unit> = future {
  instance<CommandRegistry>().get(command)?.execute()
  Unit
}

/**
 * class for registering all of the commands
 */
class CommandRegistry {
  val commands = ConcurrentHashMap<String, TransitionAction>()
  fun getEnsure(name: String): TransitionAction = commands.computeIfAbsent(name, { TransitionAction(name) })
  fun get(name: String): TransitionAction? = commands[name]
}

/**
 * injection setup for bootstrap system
 */
fun injectBootstrap() = Kodein.Module {
  bind<CommandRegistry>() with instance(CommandRegistry())
}
