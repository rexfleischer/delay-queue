package other

import java.util.concurrent.TimeUnit

fun currentTime(): Long = System.currentTimeMillis()

//fun String.execute(timeout: Long = 1000): String {
//  val proc = ProcessBuilder("bash", "-c", this)
//      .redirectOutput(ProcessBuilder.Redirect.PIPE)
//      .redirectErrorStream(true)
//      .start()
//  proc.waitFor(timeout, TimeUnit.MILLISECONDS)
//  return proc.inputStream.bufferedReader().readText()
//}
