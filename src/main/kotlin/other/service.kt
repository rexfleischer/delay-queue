package other

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.Properties
import java.util.concurrent.atomic.AtomicBoolean
import java.util.stream.Collectors

/**
 * the main entry point to the service...
 */
abstract class SimpleService(raw_config: Properties) {
  companion object {
    private val logger = LoggerFactory.getLogger(SimpleService::class.java)
  }
  
  val start_promise = Promise<Unit>()
  val shutdown_promise = Promise<Unit>()
  val is_running: Boolean get() = start_promise.isDone && !start_promise.isCompletedExceptionally && !shutdown_promise.isDone
  val configuration = Configuration(raw_config)
  
  private val start_started = AtomicBoolean(false)
  private val shutdown_started = AtomicBoolean(false)
  
  private var _kodein: Kodein? = null
  val kodein: Kodein
    get() = _kodein ?: throw IllegalStateException("system not initialized")
  
  private val internal_error = object: ShutdownWithError {
    override fun shutdown() {
      logger.error("internal error")
      shutdown()
    }
  }
  
  protected abstract fun createModules(): List<Kodein.Module>
  
  /**
   * starts the service.
   * this will automatically call shutdown if it fails to start.
   */
  fun start(): Promise<Unit> {
    if (!start_started.compareAndSet(false, true)) throw IllegalStateException("system already started")
    _kodein = createInjector()
    kodein.executeBootstrap("start").proxyFinished(start_promise)
    start_promise.whenException {
      logger.error("error during start", it)
      shutdown()
    }
    return start_promise
  }
  
  fun shutdown(): Promise<Unit> {
    if (!shutdown_started.compareAndSet(false, true)) return shutdown_promise
    start_started.set(true)
    (_kodein ?: return shutdown_promise).executeBootstrap("shutdown").proxyFinished(shutdown_promise)
    shutdown_promise.whenComplete { _, ex ->
      if (ex != null) logger.error("error during shutdown", ex)
      _kodein = null
    }
    return shutdown_promise
  }
  
  private fun createInjector(): Kodein {
    val modules = createModules()
    val names = modules.stream()
        .map<String> { m -> m::class.java.name }
        .map<String> { n -> n.substring(n.lastIndexOf('.') + 1) }
        .collect(Collectors.toList<String>())
    logger.warn("creating new system with modules: {}", names)
    return Kodein {
      bind<Configuration>() with instance(configuration)
      bind<ShutdownWithError>() with instance(internal_error)
      modules.forEach { import(it) }
      import(injectBootstrap())
    }
  }
}

/**
 * used to let the internals of a system turn it off
 */
interface ShutdownWithError {
  fun shutdown()
  
  suspend fun naughtyWrapper(logger: Logger, wrap: suspend () -> Unit) {
    // even though it's only a little bit of code that's being saved with
    // this method, it's still good to ensure consistent error handling
    try {
      wrap()
    }
    catch (ex: Exception) {
      // some very naughty stuff happened
      logger.error("unexpected error", ex)
      shutdown() // bad computer!
    }
  }
}
