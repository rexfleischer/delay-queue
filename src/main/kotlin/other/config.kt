package other

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import java.util.Properties
import java.util.concurrent.ConcurrentHashMap

fun Kodein.configuration(): Configuration = this.instance()

class ConfigurationMissingException(val missing: String) : IllegalStateException("missing configuration: $missing")

class ConfigurationDuplicateException(val duplicate: String) : IllegalStateException("config key duplicate: $duplicate")

/**
 * the keys that can be used to define a config key/value.
 * also, there is a registry for getting a list of keys. this
 * helps for debugging or writing/getting all the config values.
 */
sealed class ConfigKey<T> {
  abstract val name: String
  abstract val description: String
  abstract val default: T?
  
  class StringVal(override val name: String,
                  override val description: String,
                  override val default: String? = null)
    : ConfigKey<String>()
  
  class StringArrayVal(override val name: String,
                       override val description: String,
                       override val default: List<String>? = null)
    : ConfigKey<List<String>>()
  
  class BooleanVal(override val name: String,
                   override val description: String,
                   override val default: Boolean? = null)
    : ConfigKey<Boolean>()
  
  class DoubleVal(override val name: String,
                  override val description: String,
                  override val default: Double? = null)
    : ConfigKey<Double>()
  
  class IntVal(override val name: String,
               override val description: String,
               override val default: Int? = null)
    : ConfigKey<Int>()
  
  class LongVal(override val name: String,
                override val description: String,
                override val default: Long? = null)
    : ConfigKey<Long>()
}

/**
 * the configuration class for ensuring getting config
 * values happens consistently and easily
 */
class Configuration(private val wrapped: Properties) {
  
  /**
   * helpers for getting config values out of a property
   */
  private fun propS(name: String): String? = System.getProperty(name)
  private fun propB(name: String): Boolean? = System.getProperty(name)?.toBooleanStrict()
  private fun propD(name: String): Double? = System.getProperty(name)?.toDouble()
  private fun propI(name: String): Int? = System.getProperty(name)?.toInt()
  private fun propL(name: String): Long? = System.getProperty(name)?.toLong()
  private fun propA(name: String): List<String>? = System.getProperty(name)?.split("\\,")
  
  /**
   * helpers for getting config values out of envs
   */
  private fun envS(name: String): String? = System.getenv(envName(name))
  private fun envB(name: String): Boolean? = System.getenv(envName(name))?.toBooleanStrict()
  private fun envD(name: String): Double? = System.getenv(envName(name))?.toDouble()
  private fun envI(name: String): Int? = System.getenv(envName(name))?.toInt()
  private fun envL(name: String): Long? = System.getenv(envName(name))?.toLong()
  private fun envA(name: String): List<String>? = System.getenv(envName(name))?.split("\\,")
  
  // lets make sure to convert env names consistently
  private fun envName(name: String): String = name.toUpperCase().replace('.', '_').replace('-', '_')
  
  private fun String.toBooleanStrict(): Boolean? {
    if (this.equals("true", ignoreCase = true)) return true
    if (this.equals("false", ignoreCase = true)) return false
    throw IllegalArgumentException("unable to parse boolean: $this")
  }
  
  /**
   * general methods for finding a configuration value of a specific type.
   * the pattern goes like this:
   * 1 - check jvm properties
   * 2 - check process environment variables
   * 3 - check the configuration that is included (the 'wrapped' property)
   * 4 - a default value that is specified
   * 5 - throw a configuration exception
   *
   * this pattern allows you to:
   * 1 - know the config value is valid
   * 2 - always get it consistently
   * 3 - be able to configure the internals any way you want
   */
  private fun configGetS(wrapped: Properties, name: String, def: String? = null): String =
      propS(name) ?: envS(name) ?: wrapped.getProperty(name) ?: def ?:
      throw ConfigurationMissingException(name)
  private fun configGetB(wrapped: Properties, name: String, def: Boolean? = null): Boolean =
      propB(name) ?: envB(name) ?: wrapped.getProperty(name)?.toBooleanStrict() ?: def ?:
      throw ConfigurationMissingException(name)
  private fun configGetD(wrapped: Properties, name: String, def: Double? = null): Double =
      propD(name) ?: envD(name) ?: wrapped.getProperty(name)?.toDouble() ?: def ?:
      throw ConfigurationMissingException(name)
  private fun configGetI(wrapped: Properties, name: String, def: Int? = null): Int =
      propI(name) ?: envI(name) ?: wrapped.getProperty(name)?.toInt() ?: def ?:
      throw ConfigurationMissingException(name)
  private fun configGetL(wrapped: Properties, name: String, def: Long? = null): Long =
      propL(name) ?: envL(name) ?: wrapped.getProperty(name)?.toLong() ?: def ?:
      throw ConfigurationMissingException(name)
  private fun configGetA(wrapped: Properties, name: String, def: List<String>? = null): List<String> =
      propA(name) ?: envA(name) ?: wrapped.getProperty(name)?.split(",") ?: def ?:
      throw ConfigurationMissingException(name)
  
  /**
   * the main method for the class. this is the entry for getting
   * a config value with the given key
   */
  @Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
  fun <T> get(key: ConfigKey<T>): T = when(key) {
    is ConfigKey.StringVal      -> configGetS(wrapped, key.name, key.default)
    is ConfigKey.StringArrayVal -> configGetA(wrapped, key.name, key.default)
    is ConfigKey.BooleanVal     -> configGetB(wrapped, key.name, key.default)
    is ConfigKey.DoubleVal      -> configGetD(wrapped, key.name, key.default)
    is ConfigKey.IntVal         -> configGetI(wrapped, key.name, key.default)
    is ConfigKey.LongVal        -> configGetL(wrapped, key.name, key.default)
  } as T
}
