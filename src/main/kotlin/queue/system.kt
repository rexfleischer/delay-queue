package queue

import com.github.salomonbrys.kodein.Kodein
import other.SimpleService
import java.util.Properties

class QueueSystem(raw_config: Properties) : SimpleService(raw_config) {
  override fun createModules(): List<Kodein.Module> = listOf(
      injectHandlerListener(),
      injectMongoPersistence(),
      
      injectDbPollActor(),
      injectHandlerActor(),
      injectReceiverActor(),
      
      injectHandlerInProc()
  )
}
