package queue

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.eagerSingleton
import com.github.salomonbrys.kodein.instance
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.SendChannel
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import other.Configuration
import other.ShutdownWithError
import other.currentTime
import other.onShutdown
import other.onStart
import java.util.concurrent.TimeUnit


/**
 * this actor polls from the database and sends the messages
 * that are a configured amount of time away from being delivered
 * to the handler actor.
 */
class DbPollActor(config: Configuration,
                  private val listeners: HandlerStrategyListener,
                  private val consumer_channel: SendChannel<QueueMessage>,
                  private val message_dao: QueueMessageDAO,
                  private val shutdown: ShutdownWithError) {
  companion object {
    private val logger = LoggerFactory.getLogger(ReceiverActor::class.java)
  }
  
  private val poll_time = config.get(DELIVERY_POLL_TIME)
  private val mark_timeout = config.get(DELIVERY_MARK_TIMEOUT)
  private val poll_delay = config.get(POLLER_POLL_DELAY)
  
  private val poll_channel = Channel<Unit>(Channel.CONFLATED)
  
  fun start() {
    poller.start()
    poller_timed.start()
    listeners.listen(this::onQueueNameChange)
  }
  
  suspend fun shutdown() {
    listeners.removeListener(this::onQueueNameChange)
    poller_timed.cancel()
    poll_channel.close()
    poller_timed.join()
    poller.join()
  }
  
  /**
   * triggers a poll when queue names change
   */
  private fun onQueueNameChange() {
    poll_channel.offer(Unit)
  }
  
  /**
   * triggers a poll every poll_delay
   */
  private val poller_timed = launch(start = CoroutineStart.LAZY) {
    while (isActive) {
      poll_channel.send(Unit)
      delay(poll_delay, TimeUnit.MILLISECONDS)
    }
  }
  
  /**
   * the main poller. this will start a db poll by receiving
   * a signal from a channel. that way we can trigger a poll
   * from multiple different sources.
   *
   * NOTE: a better way may be to create a job per poll, then
   *       have the job finish by delaying and starting another
   *       job. that way we can just end the job if it needs to
   *       be started right away. the problem with that is there
   *       is potentially many messages in flight.
   */
  private val poller = launch(start = CoroutineStart.LAZY) {
    logger.debug("dbpoller started")
    // we keep track of this so if there is an error or we start to
    // shutdown, we can at least try to unmark messages.
    var current_message: QueueMessage? = null
    
    shutdown.naughtyWrapper(logger) {
      // good practice here, if the job has been signalled to stop,
      // then this flag will be false.
      while (true) {
        poll_channel.receiveOrNull() ?: break
        
        logger.info("attempting polls")
        do {
          val time = currentTime()
          val message = message_dao.pollAndMark(
              listeners.queue_names,
              time + poll_time,
              time + mark_timeout).await()
          
          if (message != null) {
            current_message = message
            consumer_channel.send(message)
            logger.debug("polled message: ${message.queue}")
            current_message = null
          }
        }
        while (message != null && !poll_channel.isClosedForSend)
        logger.debug("attempting polls finished")
      }
    }
    
    val message_check = current_message
    if (message_check != null) {
      try {
        message_dao.unmark(message_check).await()
      }
      catch (ex: Exception) {
        logger.error("unable to unmark ${message_check.id}", ex)
      }
    }
    
    logger.info("dbpoller shutdown")
  }
}

fun injectDbPollActor() = Kodein.Module {
  bind<DbPollActor>() with eagerSingleton {
    DbPollActor(instance(), instance(), instance(), instance(), instance())
  }
  
  // turn on the poller after everything else is ready
  onStart("poller", 200) { instance<DbPollActor>().start() }
  // shutdown the poller early
  onShutdown("poller", -100) { instance<DbPollActor>().shutdown() }
}
