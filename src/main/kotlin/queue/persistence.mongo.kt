package queue

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.eagerSingleton
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import com.mongodb.ServerAddress
import com.mongodb.async.SingleResultCallback
import com.mongodb.async.client.MongoClient
import com.mongodb.async.client.MongoClientSettings
import com.mongodb.async.client.MongoClients
import com.mongodb.async.client.MongoCollection
import com.mongodb.async.client.MongoDatabase
import com.mongodb.async.client.MongoIterable
import com.mongodb.client.model.Filters
import com.mongodb.client.model.FindOneAndUpdateOptions
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.ReturnDocument
import com.mongodb.client.model.Sorts
import com.mongodb.client.model.Updates
import com.mongodb.connection.ClusterSettings
import com.mongodb.connection.ConnectionPoolSettings
import com.mongodb.connection.ServerSettings
import com.mongodb.connection.SocketSettings
import com.mongodb.connection.SslSettings
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import org.bson.BsonReader
import org.bson.BsonType
import org.bson.BsonWriter
import org.bson.codecs.Codec
import org.bson.codecs.DecoderContext
import org.bson.codecs.DocumentCodecProvider
import org.bson.codecs.EncoderContext
import org.bson.codecs.ValueCodecProvider
import org.bson.codecs.configuration.CodecProvider
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.configuration.CodecRegistry
import org.slf4j.LoggerFactory
import other.Configuration
import other.Promise
import other.onShutdown
import other.onStart

fun <T> Promise<T>.proxyMongoCallback(): SingleResultCallback<T> {
  return SingleResultCallback { t, th ->
    if (th != null) {
      completeExceptionally(th)
    }
    else {
      complete(t)
    }
  }
}

fun <T> Promise<Unit>.proxyMongoUnitCallback(): SingleResultCallback<T> {
  return SingleResultCallback { _, th ->
    if (th != null) {
      completeExceptionally(th)
    }
    else {
      complete(null)
    }
  }
}

fun <T> MongoIterable<T>.intoList(): Promise<List<T>> {
  val result = Promise<MutableList<T>>()
  into(ArrayList<T>(), result.proxyMongoCallback())
  return result.thenApply { it }
}


private class QueueCodecProvider : CodecProvider {
  private val codecs = LinkedHashMap<Class<*>, Codec<*>>()
  
  init {
    register(QueueMessageCodec())
  }
  
  fun register(codec: Codec<*>) = codecs.put(codec.encoderClass, codec)
  
  override fun <T> get(clazz: Class<T>, registry: CodecRegistry): Codec<T>? {
    var current: Class<*> = clazz
    while (current != Object::class.java) {
      val check = codecs[current]
      @Suppress("UNCHECKED_CAST")
      if (check != null) return check as Codec<T>
      current.interfaces.forEach {
        val interface_check = codecs[it]
        @Suppress("UNCHECKED_CAST")
        if (interface_check != null) return interface_check as Codec<T>
      }
      current = current.superclass
    }
    return null
  }
}

/**
 * mongos driver serialization for java is... lacking
 */
private class QueueMessageCodec : Codec<QueueMessage> {
  override fun getEncoderClass(): Class<QueueMessage> = QueueMessage::class.java
  
  override fun decode(reader: BsonReader, context: DecoderContext): QueueMessage {
    val raw = readMap(reader)
    return QueueMessage(
        id = raw["_id"] as String,
        queue = raw[QueueMessage::queue.name] as String,
        marked = raw[QueueMessage::marked.name] as Long,
        delivery = raw[QueueMessage::delivery.name] as Long,
        base64Payload = raw[QueueMessage::base64Payload.name] as String
    )
  }
  
  override fun encode(writer: BsonWriter, value: QueueMessage, context: EncoderContext) {
    writer.writeStartDocument()
    writer.writeString("_id", value.id)
    writer.writeString(QueueMessage::queue.name, value.queue)
    writer.writeInt64(QueueMessage::marked.name, value.marked)
    writer.writeInt64(QueueMessage::delivery.name, value.delivery)
    writer.writeString(QueueMessage::base64Payload.name, value.base64Payload)
    writer.writeEndDocument()
  }
  
  private fun readMap(reader: BsonReader): Map<String, Any?> {
    reader.readStartDocument()
    val read = LinkedHashMap<String, Any?>()
    while (true) {
      val type = reader.readBsonType()
      if (type == BsonType.END_OF_DOCUMENT) break
      val name = reader.readName()
      read[name] = readValue(reader, type)
    }
    reader.readEndDocument()
    return read
  }
  
  private fun readArray(reader: BsonReader): List<Any?> {
    reader.readStartArray()
    val read = ArrayList<Any?>()
    while (true) {
      val type = reader.readBsonType()
      if (type == BsonType.END_OF_DOCUMENT) break
      read.add(readValue(reader, type))
    }
    reader.readEndArray()
    return read
  }
  
  private fun readValue(reader: BsonReader, type: BsonType): Any? {
    when (type) {
      BsonType.DOCUMENT              -> return readMap(reader)
      BsonType.ARRAY                 -> return readArray(reader)
      BsonType.BOOLEAN               -> return reader.readBoolean()
      BsonType.INT32                 -> return reader.readInt32()
      BsonType.INT64                 -> return reader.readInt64()
      BsonType.DOUBLE                -> return reader.readDouble()
      BsonType.DECIMAL128            -> TODO()
      BsonType.STRING                -> return reader.readString()
      BsonType.BINARY                -> return reader.readBinaryData().data
      BsonType.OBJECT_ID             -> return reader.readObjectId()
      BsonType.DATE_TIME             -> return reader.readDateTime()
      BsonType.TIMESTAMP             -> return reader.readTimestamp().time * 1000
      BsonType.NULL                  -> {
        reader.readNull()
        return null
      }
      BsonType.UNDEFINED             -> TODO()
      BsonType.END_OF_DOCUMENT       -> TODO()
      BsonType.SYMBOL                -> TODO()
      BsonType.DB_POINTER            -> TODO()
      BsonType.JAVASCRIPT_WITH_SCOPE -> TODO()
      BsonType.JAVASCRIPT            -> TODO()
      BsonType.REGULAR_EXPRESSION    -> TODO()
      BsonType.MIN_KEY               -> TODO()
      BsonType.MAX_KEY               -> TODO()
    }
  }
  
}


class MongoManager(config: Configuration) {
  
  val hostname = config.get(MONGO_HOSTNAME)
  val port = config.get(MONGO_PORT)
  val database_name = config.get(MONGO_DATABASE_NAME)
  val queue_collection_name = config.get(MONGO_COLLECTION_QUEUE_NAME)
  val quarantine_collection_name = config.get(MONGO_COLLECTION_QUARANTINE_NAME)
  
  suspend fun start() {
    val hosts = ArrayList<ServerAddress>()
    hosts.add(ServerAddress(hostname, port))
    val registry = CodecRegistries.fromProviders(
        // standard codecs
        ValueCodecProvider(),
        DocumentCodecProvider(),
        // our codecs
        QueueCodecProvider())
    val settings = MongoClientSettings
        .builder()
        .codecRegistry(registry)
        .clusterSettings(ClusterSettings.builder().hosts(hosts).build())
        .connectionPoolSettings(ConnectionPoolSettings.builder().build())
        .serverSettings(ServerSettings.builder().build())
        .socketSettings(SocketSettings.builder().build())
        .sslSettings(SslSettings.builder().enabled(false).build())
        .build()
    _client = MongoClients.create(settings)
    ensureCollection(queue_collection)
    ensureCollection(quarantine_collection)
  }
  
  fun shutdown() {
    _client?.close()
    _client = null
  }
  
  private var _client: MongoClient? = null
  val client: MongoClient
    get() = _client ?: throw IllegalStateException("client not started")
  
  val database: MongoDatabase
    get() = client.getDatabase(database_name)
  
  val queue_collection: MongoCollection<QueueMessage>
    get() = database.getCollection(queue_collection_name, QueueMessage::class.java)
  
  val quarantine_collection: MongoCollection<QueueMessage>
    get() = database.getCollection(quarantine_collection_name, QueueMessage::class.java)
  
  private suspend fun ensureCollection(collection: MongoCollection<QueueMessage>) {
    Promise<String>().also {
      collection.createIndex(Indexes.ascending(QueueMessage::delivery.name), it.proxyMongoCallback())
    }.await()
    Promise<String>().also {
      collection.createIndex(Indexes.ascending(QueueMessage::marked.name), it.proxyMongoCallback())
    }.await()
    Promise<String>().also {
      collection.createIndex(Indexes.ascending(QueueMessage::queue.name, QueueMessage::delivery.name),
          it.proxyMongoCallback())
    }.await()
    Promise<String>().also {
      collection.createIndex(Indexes.ascending(QueueMessage::queue.name, QueueMessage::marked.name),
          it.proxyMongoCallback())
    }.await()
  }
}

abstract class MongoMultiQueueDAO : MultiQueueDAO {
  
  protected abstract val collection: MongoCollection<QueueMessage>
  
  override fun queueNames(): Promise<List<String>> {
    return Promise<List<String>>().also { collection.distinct(QueueMessage::queue.name, String::class.java) }
  }
  
  override fun queueSize(queue: String): Promise<Long> {
    return Promise<Long>().also {
      collection.count(Filters.eq(QueueMessage::queue.name, queue),
          it.proxyMongoCallback())
    }
  }
  
  override fun queueSizes(): Promise<Map<String, Long>> = future {
    queueNames().await()
        .map { it to queueSize(it) }
        .map { it.first to it.second.await() }
        .toMap()
  }
}

class MongoQueueMessageDAO(private val manager: MongoManager) : MongoMultiQueueDAO(),
                                                                QueueMessageDAO {
  
  companion object {
    private val logger = LoggerFactory.getLogger(MongoQueueMessageDAO::class.java)
  }
  
  override val collection: MongoCollection<QueueMessage>
    get() = manager.queue_collection
  
  override fun finished(message: QueueMessage): Promise<Unit> {
    return Promise<Unit>().also { collection.deleteOne(Filters.eq("_id", message.id), it.proxyMongoUnitCallback()) }
  }
  
  override fun unmark(message: QueueMessage): Promise<Unit> {
    return Promise<Unit>().also {
      collection.updateOne(
          Filters.eq("_id", message.id),
          Updates.set(QueueMessage::marked.name, 0L),
          it.proxyMongoUnitCallback()
      )
    }
  }
  
  override fun unmarkMany(messages: Collection<QueueMessage>): Promise<Unit> {
    return Promise<Unit>().also {
      collection.updateMany(
          Filters.`in`("_id", messages.map { it.id }),
          Updates.set(QueueMessage::marked.name, 0L),
          it.proxyMongoUnitCallback()
      )
    }
  }
  
  override fun pollAndMark(queues: Collection<String>,
                           until_time: Long,
                           mark_timeout_at: Long): Promise<QueueMessage?> {
    return Promise<QueueMessage?>().also {
      collection.findOneAndUpdate(
          Filters.and(
              Filters.`in`(QueueMessage::queue.name, queues),
              Filters.eq(QueueMessage::marked.name, 0L),
              Filters.lte(QueueMessage::delivery.name, until_time)),
          Updates.set(QueueMessage::marked.name, mark_timeout_at),
          FindOneAndUpdateOptions()
              .returnDocument(ReturnDocument.AFTER)
              .sort(Sorts.ascending(QueueMessage::delivery.name)),
          it.proxyMongoCallback()
      )
    }
  }
  
  override fun insert(message: QueueMessage): Promise<Unit> {
    return Promise<Unit>().also { collection.insertOne(message, it.proxyMongoUnitCallback()) }
  }
  
  override fun insertMany(messages: Collection<QueueMessage>): Promise<Unit> {
    return Promise<Unit>().also {
      collection.insertMany(messages as MutableList<out QueueMessage>,
          it.proxyMongoUnitCallback())
    }
  }
}

class MongoQuarantineDAO(private val manager: MongoManager) : MongoMultiQueueDAO(),
                                                              QuarantineDAO {
  
  override val collection: MongoCollection<QueueMessage>
    get() = manager.queue_collection
  
  override fun insert(message: QueueMessage): Promise<Unit> {
    return Promise<Unit>().also { collection.insertOne(message, it.proxyMongoUnitCallback()) }
  }
  
  override fun remove(id: String): Promise<Unit> {
    return Promise<Unit>().also { collection.deleteOne(Filters.eq("_id", id), it.proxyMongoUnitCallback()) }
  }
  
  override fun page(queue: String, offset: Long, size: Int): Promise<List<QueueMessage>> {
    return collection
        .find(Filters.eq(QueueMessage::queue.name, queue))
        .sort(Sorts.ascending(QueueMessage::delivery.name))
        .skip(offset.toInt())
        .limit(size)
        .intoList()
  }
}

fun injectMongoPersistence() = Kodein.Module {
  bind<MongoManager>() with eagerSingleton { MongoManager(instance()) }
  bind<QueueMessageDAO>() with provider { MongoQueueMessageDAO(instance()) }
  bind<MongoQueueMessageDAO>() with provider { MongoQueueMessageDAO(instance()) }
  bind<QuarantineDAO>() with provider { MongoQuarantineDAO(instance()) }
  bind<MongoQuarantineDAO>() with provider { MongoQuarantineDAO(instance()) }
  
  onStart("mongo", -100) { instance<MongoManager>().start() }
  onShutdown("mongo", 1000) { instance<MongoManager>().shutdown() }
}
