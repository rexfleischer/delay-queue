package queue

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.eagerSingleton
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.SendChannel
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import other.Configuration
import other.ShutdownWithError
import other.currentTime
import other.onShutdown
import other.onStart
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit


/**
 * this actor handles the actual job of attempting to
 * deliver a message.
 */
class HandlerActor(config: Configuration,
                   private val handle_strategy: HandlerStrategy,
                   private val quarantine_dao: QuarantineDAO,
                   private val message_dao: QueueMessageDAO,
                   private val shutdown: ShutdownWithError) {
  companion object {
    private val logger = LoggerFactory.getLogger(HandlerActor::class.java)
  }
  
  private val inbound_channel = Channel<QueueMessage>(config.get(HANDLER_CHANNEL_SIZE))
  val channel get() = inbound_channel as SendChannel<QueueMessage>
  private val children = ConcurrentHashMap.newKeySet<Job>()
  
  private val failure_delay = config.get(HANDLER_FAILURE_DELAY)
  private val failures_allowed = config.get(HANDLER_FAILURES_ALLOWED)
  
  fun start() {
    handler_receiver.start()
  }
  
  suspend fun shutdown() {
    inbound_channel.close()
    
    handler_receiver.join()
    
    // now we have to wait for the children to finish...
    // dont know of a "better" algorithm for this, so lets just
    // keep it super simple and say good night.
    while (children.isNotEmpty()) {
      delay(1)
    }
  }
  
  private val handler_receiver = launch(start = CoroutineStart.LAZY) {
    logger.debug("handler started")
    shutdown.naughtyWrapper(logger) {
      while (isActive) {
        val message = inbound_channel.receiveOrNull() ?: break
        // all we really need to do is start the handler job, because
        // that job will deal with the exact time to deliver
        startHandleJob(message)
      }
    }
    logger.info("handler shutdown")
  }
  
  private fun startHandleJob(message: QueueMessage) = launch {
    logger.debug("starting job: ${message.queue} -> ${message.id}")
    this@HandlerActor.children += this as Job
    try {
      // delay until delivery
      val initial_delay = message.delivery - currentTime()
      if (initial_delay > 0) delay(initial_delay)
      
      var failures_left = failures_allowed
      do {
        logger.debug("attempting handle: ${message.queue} -> ${message.id}")
        val result = handle_strategy.handle(message)
        val keep_going: Boolean = when (result) {
          is MessageHandleResult.Success     -> {
            logger.debug("message successfully handled: ${message.queue} -> ${message.id}")
            message_dao.finished(message).await()
            false
          }
          is MessageHandleResult.Failure     -> {
            failures_left--
            if (failures_left < 0) {
              logger.info("failure count reached. quarantine message: $result")
              listOf(
                  quarantine_dao.insert(message),
                  message_dao.finished(message)
              ).forEach { it.await() }
              false
            }
            else {
              val delay_for = result.retry_delay_override ?: failure_delay
              logger.info("handler signalled failure. delay ($delay_for) then retry: $result")
              println("before")
              delay(delay_for, TimeUnit.MILLISECONDS)
              println("after")
              true
            }
          }
          is MessageHandleResult.Quarantine  -> {
            logger.info("handler signalled to quarantine: $result")
            listOf(
                quarantine_dao.insert(message),
                message_dao.finished(message)
            ).forEach { it.await() }
            false
          }
          is MessageHandleResult.NoListeners -> {
            logger.warn("not handlers available for message: ${message.queue}")
            message_dao.unmark(message).await()
            false
          }
        }
      }
      while (keep_going && !inbound_channel.isClosedForReceive)
    }
    catch (ex: Exception) {
      logger.error("internal error during message handle: ${message.id}")
      try {
        message_dao.unmark(message).await()
      }
      catch (ex2: Exception) {
        logger.error("unable to unmark after internal error", ex2)
      }
    }
    finally {
      this@HandlerActor.children -= this as Job
    }
  }
}

fun injectHandlerActor() = Kodein.Module {
  bind<HandlerActor>() with eagerSingleton {
    HandlerActor(instance(), instance(), instance(), instance(), instance())
  }
  bind<SendChannel<QueueMessage>>() with provider { instance<HandlerActor>().channel }
  
  // start the handler early
  onStart("handler", 0) { instance<HandlerActor>().start() }
  // end the handler late
  onShutdown("handler", 200) { instance<HandlerActor>().shutdown() }
}
