package queue

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.eagerSingleton
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ClosedSendChannelException
import kotlinx.coroutines.experimental.channels.SendChannel
import kotlinx.coroutines.experimental.channels.sendBlocking
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import other.Configuration
import other.Promise
import other.ShutdownWithError
import other.SimpleService
import other.currentTime
import other.onShutdown
import other.onStart
import java.util.UUID

data class QueueRequest(val queue: String,
                        val delivery: Long,
                        val base64Payload: String,
                        val promise: Promise<QueueMessage> = Promise())

/**
 * this is the common actor for handling the queueing of a message.
 * all messages that enter the system needs to be sent here.
 * the general process of receiving a message is this:
 * 1 - figure out if this message needs to be "marked"
 * 2 - persist the message
 * 3 - if marked, send the message to be processed
 *
 * there is a configurable amount of receivers in this actor.
 */
class ReceiverActor(config: Configuration,
                    private val listeners: HandlerStrategyListener,
                    private val consumer_channel: SendChannel<QueueMessage>,
                    private val message_dao: QueueMessageDAO,
                    private val shutdown: ShutdownWithError) {
  companion object {
    private val logger = LoggerFactory.getLogger(ReceiverActor::class.java)
  }
  
  private val request_channel = Channel<QueueRequest>(config.get(RECEIVER_CHANNEL_SIZE))
  val channel get() = request_channel as SendChannel<QueueRequest>
  private val receivers = (1..config.get(RECEIVER_RECEIVER_COUNT)).map { createReceiver() }
  private val immediate_grace: Long by lazy { config.get(DELIVERY_IMMEDIATE_GRACE) }
  private val poll_time: Long by lazy { config.get(DELIVERY_POLL_TIME) }
  private val mark_timeout by lazy { config.get(DELIVERY_MARK_TIMEOUT) }
  
  
  fun start() {
    // just start the receivers
    receivers.forEach { job -> job.start() }
  }
  
  suspend fun shutdown() {
    // close the channel so we cannot accept any more requests.
    // NOTE: this only closes the send channel. the receive channel
    //       does not close until all the messages are consumed, which
    //       will cause a CancellationException to be throw, then will
    //       stop the receiver
    request_channel.close()
    
    // now wait for all the receivers to finish.
    receivers.forEach { it.join() }
  }
  
  private fun QueueRequest.figureMarked(time: Long): Long {
    return if (listeners.queue_names.contains(this.queue) && time + poll_time > this.delivery)
      time + immediate_grace + mark_timeout
    else 0
  }
  
  private fun createReceiver() = launch(start = CoroutineStart.LAZY) {
    logger.debug("receiver started")
    var current_message: QueueRequest? = null
    
    shutdown.naughtyWrapper(logger) {
      // good practice here, if the job has been signalled to stop,
      // then this flag will be false.
      while (isActive) {
        val message = request_channel.receiveOrNull() ?: break
        current_message = message
        logger.debug("received message: queue({}) -> delivery({}) -> length({})",
            message.queue, message.delivery, message.base64Payload.length)
        
        // create the message that we are going to deal with
        val time = currentTime()
        val inserting = QueueMessage(
            id = UUID.randomUUID().toString(),
            queue = message.queue,
            delivery = message.delivery,
            base64Payload = message.base64Payload,
            marked = message.figureMarked(time)
        )
        
        // insert it to ensure that it is persisted before we do anything
        // else with the message
        message_dao.insert(inserting).await()
        
        // if this message is marked, then we need to send it to a consumer
        // right now.
        if (inserting.marked != 0L) {
          try {
            consumer_channel.send(inserting)
          }
          catch (ex: ClosedSendChannelException) {
            // this means that the message could not be sent and that
            // the system is likely shutting down. so unmark it because
            // we couldnt do anything with it.
            message_dao.unmark(inserting).await()
          }
        }
        
        // notify the message sender that we are finished consuming this message
        message.promise.complete(null)
        current_message = null
      }
    }
    
    // make sure to send an exception if there is a current message
    current_message?.promise?.completeExceptionally(IllegalStateException("going away"))
    logger.info("receiver shutdown")
  }
  
}

/**
 * used to queue a message while in process
 */
fun SimpleService.queueMessage(request: QueueRequest): Promise<QueueMessage> {
  kodein.instance<SendChannel<QueueRequest>>().sendBlocking(request)
  return request.promise
}

fun injectReceiverActor() = Kodein.Module {
  bind<ReceiverActor>() with eagerSingleton {
    ReceiverActor(instance(), instance(), instance(), instance(), instance())
  }
  bind<SendChannel<QueueRequest>>() with provider { instance<ReceiverActor>().channel }
  
  // turn on the receiver after all other services have started
  onStart("receiver", 100) { instance<ReceiverActor>().start() }
  
  // shutdown the receiver early
  onShutdown("receiver", -100) { instance<ReceiverActor>().shutdown() }
}
