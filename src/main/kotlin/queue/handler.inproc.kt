package queue

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import kotlinx.coroutines.experimental.future.await
import other.Promise
import other.SimpleService
import java.util.concurrent.ConcurrentHashMap

internal class InProcHandlerStrategy(private val handles: HandlerStrategyListener) : HandlerStrategy {
  
  private val listeners = ConcurrentHashMap<String, (QueueMessage) -> Promise<MessageHandleResult>>()
  
  internal fun register(queue: String, handler: (QueueMessage) -> Promise<MessageHandleResult>) {
    if (listeners.putIfAbsent(queue, handler) != null) throw IllegalStateException("already registered: $queue")
    handles.setHandling(listeners.keys)
  }
  
  internal fun unregister(queue: String) {
    listeners.remove(queue)
    handles.setHandling(listeners.keys)
  }
  
  override suspend fun handle(message: QueueMessage): MessageHandleResult {
    val handler = listeners[message.queue] ?: return MessageHandleResult.NoListeners
    return handler(message).await()
  }
}

fun SimpleService.addInProcHandler(queue: String, handler: (QueueMessage) -> Promise<MessageHandleResult>) {
  kodein.instance<InProcHandlerStrategy>().register(queue, handler)
}

fun SimpleService.removeInProcHandler(queue: String) {
  kodein.instance<InProcHandlerStrategy>().unregister(queue)
}

fun injectHandlerInProc() = Kodein.Module {
  bind<HandlerStrategy>() with singleton { instance<InProcHandlerStrategy>() as HandlerStrategy }
  bind<InProcHandlerStrategy>() with singleton { InProcHandlerStrategy(instance()) }
}
