package queue

import other.Promise


/**
 * the data model for delay queue message in a database
 */
data class QueueMessage(
    /**
     * the id of a message
     */
    val id: String,
    /**
     * the queue this message belongs to
     */
    val queue: String,
    /**
     * there are two states:
     * 1 - if marked is 0. this means that it is ok to grab by a consumer
     * 2 - if marked is epoch. this means the message is queued in memory
     *     already. but it also defines when this message needs to be consumed
     *     by. so if the message is not "finished" by the this time plus
     *     whatever the grace period is, then it should be picked up by
     *     another process to attempt to handle the bad state of this message
     */
    val marked: Long,
    /**
     * when the desired time of delivery is.
     *
     * 0 means immediate.
     */
    val delivery: Long,
    /**
     * the actual payload of the message.
     */
    val base64Payload: String)

/**
 * common queue management for the daos
 */
interface MultiQueueDAO {
  fun queueNames(): Promise<List<String>>
  fun queueSizes(): Promise<Map<String, Long>>
  fun queueSize(queue: String): Promise<Long>
}

/**
 * the main interface for persisting messages and handling
 * the sending
 */
interface QueueMessageDAO : MultiQueueDAO {
  /**
   * let the database know that this message has been processed.
   * this will allow the database to delete or otherwise cleanup
   * the message.
   */
  fun finished(message: QueueMessage): Promise<Unit>
  
  /**
   * if an error happens or a message cannot be processed (like
   * the system shutting down) then the message is unmarked
   * so another consumer can pick up the message.
   */
  fun unmark(message: QueueMessage): Promise<Unit>
  
  /**
   * same as other unmark, but for a lot of ids
   */
  fun unmarkMany(messages: Collection<QueueMessage>): Promise<Unit>
  
  /**
   * finds messages with a mark of 0 and a delivery LTE to until_time. it should
   * mark every returned message with mark_timeout_at.
   */
  fun pollAndMark(queues: Collection<String>,
                  until_time: Long,
                  mark_timeout_at: Long): Promise<QueueMessage?>
  
  /**
   * insert a single message into the queue.
   */
  fun insert(message: QueueMessage): Promise<Unit>
  
  /**
   * insert many messages into the queue.
   */
  fun insertMany(messages: Collection<QueueMessage>): Promise<Unit>
}

/**
 * this is a simple quarantine dao. its not really meant for this
 * system to deal with them. but this service can at least show some
 * of the info
 */
interface QuarantineDAO : MultiQueueDAO {
  fun insert(message: QueueMessage): Promise<Unit>
  fun remove(id: String): Promise<Unit>
  fun page(queue: String, offset: Long, size: Int): Promise<List<QueueMessage>>
}
