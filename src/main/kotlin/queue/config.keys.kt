package queue

import other.ConfigKey


val RECEIVER_RECEIVER_COUNT = ConfigKey.IntVal(
    "receiver.receiver-count",
    "the amount of receivers", 8)
val RECEIVER_CHANNEL_SIZE = ConfigKey.IntVal(
    "receiver.channel-size",
    "the size of the request channel before blocking occurs", 256)

val POLLER_POLL_DELAY = ConfigKey.LongVal(
    "poller.poll-delay",
    "the amount of time to pause between poll attempts", 20_000)

val DELIVERY_IMMEDIATE_GRACE = ConfigKey.LongVal(
    "receiver.immediate-grace-time",
    "the extra mark time added on if a received message should have already been delivered", 1000)
val DELIVERY_POLL_TIME = ConfigKey.LongVal(
    "delivery.poll-time",
    "the amount of time before a message needs to be delivered when it should be brought into memory", 30_000)
val DELIVERY_MARK_TIMEOUT = ConfigKey.LongVal(
    "delivery.mark-timeout",
    "the timeout before a message has not been deleted after it has been polled, where it is considered an error", 5_000)

val HANDLER_CHANNEL_SIZE = ConfigKey.IntVal(
    "handler.channel-size",
    "the size of the channel before blocking happens. it may be important to keep this down to avoid thrashing", 8)
val HANDLER_FAILURE_DELAY = ConfigKey.LongVal(
    "handler.failure-delay",
    "the default delay for a failure before trying again", 10_000)
val HANDLER_FAILURES_ALLOWED = ConfigKey.IntVal(
    "handler.failures-allowed",
    "the amount of failures a message can have before its automatically quarantined", 3)

val MONGO_HOSTNAME = ConfigKey.StringVal(
    "mongo.hostname",
    "the mongo database hostname", "192.168.99.100")
val MONGO_PORT = ConfigKey.IntVal(
    "mongo.port",
    "the mongo database port", 27017)
val MONGO_DATABASE_NAME = ConfigKey.StringVal(
    "mongo.database-name",
    "the name of the mongo database this queue will use", "delay_queue")
val MONGO_COLLECTION_QUEUE_NAME = ConfigKey.StringVal(
    "mongo.collection-name",
    "the name of the mongo collection this queue will use", "queue")
val MONGO_COLLECTION_QUARANTINE_NAME = ConfigKey.StringVal(
    "mongo.collection-quarantine-name",
    "the name of the mongo collection for quarantined messages", "quarantine")
