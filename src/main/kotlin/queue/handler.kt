package queue

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.eagerSingleton
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap

/**
 * the responses from the handle strategy
 */
sealed class MessageHandleResult {
  /**
   * the consumption of the message was successful
   */
  object Success : MessageHandleResult()
  
  /**
   * the consumption of the message failed with the given reason.
   * but we want to retry the message.
   */
  data class Failure(val reason: String, val retry_delay_override: Long? = null) : MessageHandleResult()
  
  /**
   * the consumption of the message failed and should not be retried.
   */
  data class Quarantine(val reason: String) : MessageHandleResult()
  
  /**
   * this means the message could not be handled because there
   * are not listeners available for the given queue.
   */
  object NoListeners : MessageHandleResult()
}

/**
 * the entry point for handling messages with the given strategy
 */
interface HandlerStrategy {
  suspend fun handle(message: QueueMessage): MessageHandleResult
}

/**
 * and internal helper so we can change which queues this system
 * knows to handle.
 */
class HandlerStrategyListener {
  companion object {
    private val logger = LoggerFactory.getLogger(HandlerStrategyListener::class.java)
  }
  
  private val listeners = ConcurrentHashMap.newKeySet<() -> Unit>()
  private val names = ConcurrentHashMap.newKeySet<String>()
  var queue_names: Set<String> = emptySet()
    private set
  
  fun modHandling(adding: Collection<String> = emptyList(), removing: Collection<String> = emptyList()) {
    names += adding
    names -= removing
    notifyListeners()
  }
  
  fun setHandling(new_names: Collection<String>) {
    names.clear()
    names += new_names
    notifyListeners()
  }
  
  private fun notifyListeners() {
    queue_names = names.toSet()
    this.listeners.toSet().forEach {
      try {
        it()
      }
      catch (ex: Exception) {
        logger.error("unhandled exception during notify", ex)
      }
    }
  }
  
  fun listen(handler: () -> Unit) = listeners.add(handler)
  fun removeListener(handler: () -> Unit) = listeners.remove(handler)
}

fun injectHandlerListener() = Kodein.Module {
  bind<HandlerStrategyListener>() with eagerSingleton { HandlerStrategyListener() }
}
